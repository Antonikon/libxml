#include "libxml.h"

TXMLTag::TXMLTag(const std::string &filename):
    TXMLItem(EXMLItemType::Tag)
{
    std::ifstream stream(filename);
    Parse(stream);
    stream.close();
}

void TXMLTag::Save(std::ostream &stream) {
    if (!Name.empty()) {
        stream << "<" << SerializeText(Name);
        for (const auto &param: PropertyMap)
            stream << " " << SerializeText(param.first) << " = \"" << param.second << "\"";
        if (ItemList.empty()) {
            stream << "/>";
        } else {
            stream << ">";
            for (auto &ptr: ItemList) ptr->Save(stream);
            stream << "</" << SerializeText(Name) << ">";
        }
    } else for (auto &ptr: ItemList) ptr->Save(stream);
}

TXMLTag::TXMLTag():
    TXMLItem(EXMLItemType::Tag)
{
}

TXMLTag::TXMLTag(const TXMLTag &other):
    TXMLItem(EXMLItemType::Tag)
{
    *this = other;
}

void TXMLTag::Parse(std::istream &stream) {
    std::string tagNameBuffer;
    std::string paramNameBuffer;
    std::string buffer;
    std::unordered_map <std::string, std::string> paramBuffer;

    EParserState state = EParserState::Text;

    TXMLTag *current = this;
    size_t index = 0;
    int ch = stream.get();
    int closeState = 0; //0 - open, 1 - close 2 - self-close

    while (ch != -1) {
        switch (state) {
        case EParserState::Text: {
            if (ch == '<') {
                closeState = 0;
                state = EParserState::TagName;
                if (!buffer.empty()) current->ItemList.push_back(std::unique_ptr<TXMLItem>(new TXMLText(DeserializeText(buffer))));
                buffer.clear();
            } else buffer.push_back(ch);
            break;
        }
        case EParserState::WaitPropertyName:
            if (ch == '/') closeState = 2;
            else if (ch == '>') {
                TXMLTag *tag = new TXMLTag();
                tag->Parent = current;
                tag->Name = tagNameBuffer;
                tag->PropertyMap = std::move(paramBuffer);
                current->ItemList.push_back(std::unique_ptr<TXMLItem>(tag));
                state = EParserState::Text;
                if (closeState == 0) current = tag;
                break;
            } else if (!isspace(ch)) state = EParserState::PropertyName;
            else if (ch == '\"' || ch == '\'' || ch == '=' || ch == '<') {
                throw std::runtime_error("unexpected symbol " + std::to_string(index));
            } else break;
        case EParserState::PropertyName:
            if (ch == ' ' || ch == '=') {
                state = EParserState::WaitPropertyValue;
                paramNameBuffer = DeserializeText(buffer);
                buffer.clear();
            } else buffer.push_back(ch);
            break;
        case EParserState::WaitPropertyValue:
            if (ch == '\"' || ch == '\'') state = EParserState::PropertyValue;
            else if (ch == '/' || ch == '<')
                throw std::runtime_error("unexpected symbol " + std::to_string(index));
            break;
        case EParserState::PropertyValue:
            if (ch == '\"' || ch == '\'')  {
                paramBuffer.insert(std::make_pair(paramNameBuffer, buffer));
                buffer.clear();
                paramNameBuffer.clear();
                state = EParserState::WaitPropertyName;
             } else buffer.push_back(ch);
            break;
        case EParserState::TagName:
            if (ch == '!' && buffer.empty()) {
                int dashCount = 0;
                for (int i = 0; i < 2; i++) {
                    index++;
                    ch = stream.get();
                    if (ch != '-')
                        throw std::runtime_error("unexpected symbol " + std::to_string(index));
                }
                do {
                    ch = stream.get();
                    index++;
                    buffer.push_back(ch);
                    if (ch == '>' && dashCount >= 2) break;
                    else if (ch == '-') dashCount++;
                    else dashCount = 0;
                } while (ch != -1);
                buffer.erase(buffer.size() - 3, 3);
                state = EParserState::Text;
                TXMLText &comment = current->AddText(buffer, current->end());
                comment.SetComment(true);
                buffer.clear();
            } else if (ch == '/') {
                if (buffer.empty()) closeState = 1;
                else {
                    closeState = 2;
                }
            } else if (ch == ' ' || ch == '>') {
                tagNameBuffer = DeserializeText(buffer);
                buffer.clear();
                if (ch == ' ') state = EParserState::WaitPropertyName;
                else if (ch == '>') {
                    if (closeState != 1) {
                        TXMLTag *tag = new TXMLTag();
                        tag->Name = tagNameBuffer;
                        tag->Parent = current;
                        current->ItemList.push_back(std::unique_ptr<TXMLItem>(tag));
                        if (closeState == 0) current = tag;
                    }
                    state = EParserState::Text;
                }
                if (closeState == 1) {
                    if (current->Name != tagNameBuffer)
                        throw std::runtime_error(current->Name + " isn't closed at " + std::to_string(index));
                    current = current->Parent;
                }
            } else buffer.push_back(ch);
            break;
        }
        index++;
        ch = stream.get();
    }
    if (!buffer.empty()) AddText(buffer, ItemList.end());
}

void TXMLTag::SaveToFile(const std::string &filename) {
    std::ofstream stream(filename);
    Save(stream);
    stream.close();
}

std::string TXMLTag::SaveToString() {
    std::stringstream stream;
    Save(stream);
    return stream.str();
}

const std::string &TXMLTag::GetProperty(const std::string &property) {
    return PropertyMap.at(property);
}

TXMLText &TXMLTag::AddText(const std::string &text, TXMLIterator position) {
    TXMLText *item = new TXMLText(text);
    ItemList.insert(position, std::unique_ptr<TXMLItem>(item));
    return *item;
}

TXMLTag &TXMLTag::AddTag(const std::string &name, TXMLIterator position) {
    TXMLTag *tag = new TXMLTag();
    tag->Name = name;
    ItemList.insert(position, std::unique_ptr<TXMLItem>(tag));
    return *tag;
}

TXMLIterator TXMLTag::Find(const std::string &name) {
    return Find(name, ItemList.begin());
}

TXMLIterator TXMLTag::Find(const std::string &name, const TXMLIterator &position) {
    for (auto it = position; it != ItemList.end(); it++) {
        TXMLItem &item = **it;
        if (item.Type != EXMLItemType::Tag) continue;
        TXMLTag &tag = item;
        if (tag.Name == name) return it;
    }
    throw std::runtime_error(Name + " doesn't contains " + name);
}

void TXMLTag::SetFromRawData(const std::string &string) {
    std::stringstream stream(string);
    Parse(stream);
}

TXMLIterator TXMLTag::begin() {
    return ItemList.begin();
}

TXMLIterator TXMLTag::end() {
    return ItemList.end();
}

TXMLReverseIterator TXMLTag::rbegin() {
    return ItemList.rbegin();
}

TXMLReverseIterator TXMLTag::rend() {
    return ItemList.rend();
}

TXMLPropertyIterator TXMLTag::begin_property() {
    return PropertyMap.begin();
}

TXMLPropertyIterator TXMLTag::end_property() {
    return PropertyMap.end();
}

const std::string &TXMLText::GetText() const {
    return Text;
}

void TXMLText::SetText(const std::string &text) {
    Text = text;
}

TXMLText &TXMLText::operator =(const std::string &text) {
    Text = text;
    return *this;
}

bool TXMLText::IsComment() const {
    return Comment;
}

void TXMLText::SetComment(bool value) {
    Comment = value;
}

void TXMLText::Save(std::ostream &stream) {
    if (Comment) stream << "<!--" << SerializeText(Text) << "-->";
    else stream << SerializeText(Text);
}

TXMLItem::operator TXMLTag&() {
    return *(TXMLTag*)(this);
}

TXMLItem::operator TXMLText&() {
    return *(TXMLText*)(this);
}

void TXMLTag::SetProperty(const std::string &name, const std::string &value) {
    PropertyMap[name] = value;
}

TXMLIterator TXMLTag::Erase(const TXMLIterator &iterator) {
    return ItemList.erase(iterator);
}

std::string &TXMLTag::operator [](const std::string &name) {
    return PropertyMap[name];
}

TXMLTag &TXMLTag::operator =(const std::string &rawData) {
    SetFromRawData(rawData);
    return *this;
}

TXMLTag &TXMLTag::operator =(const TXMLTag &other) {
    Name = other.Name;
    PropertyMap = other.PropertyMap;
    ItemList.clear();
    for (auto &ptr: other.ItemList) {
        TXMLItem &item = *ptr;
        if (item.GetType() == EXMLItemType::Text) {
            TXMLText *text = new TXMLText(item.ToText().GetText());
            text->SetComment(item.ToText().IsComment());
            text->Parent = this;
            ItemList.push_back(std::unique_ptr<TXMLItem>(text));
        } else if (item.GetType() == EXMLItemType::Tag) {
            TXMLTag *tag = new TXMLTag(item.ToTag());
            tag->Parent = this;
            ItemList.push_back(std::unique_ptr<TXMLItem>(tag));
        }
    }
    return *this;
}

int TXMLTag::GetPropertyCount() const {
    return PropertyMap.size();
}

int TXMLTag::GetItemCount() const {
    return ItemList.size();
}

const std::string TXMLTag::GetName() const {
    return Name;
}

void TXMLTag::SetName(const std::string &name) {
    Name = name;
}

TXMLTag &TXMLItem::ToTag() {
    return *this;
}

TXMLText &TXMLItem::ToText() {
    return *this;
}

TXMLTag &TXMLItem::GetParent() {
    return *Parent;
}

std::string TXMLItem::SerializeText(std::string text) {
    const std::vector <std::pair<char, std::string> > dict = {
        std::make_pair('<', "&lt"),
        std::make_pair('>', "&gt"),
        std::make_pair('&', "&amp"),
        std::make_pair('\'', "&apos"),
        std::make_pair('\"', "&quot"),
    };
    for (size_t i = 0; i < text.size(); i++) {
        for (size_t j = 0; j < dict.size(); j++) {
            if (text[i] == dict[j].first) {
                text.replace(i, 1, dict[j].second);
                break;
            }
        }
    }
    return text;
}

std::string TXMLItem::DeserializeText(std::string text) {
    const std::vector <std::pair<char, std::string> > dict = {
        std::make_pair('<', "&lt"),
        std::make_pair('>', "&gt"),
        std::make_pair('&', "&amp"),
        std::make_pair('\'', "&apos"),
        std::make_pair('\"', "&quot"),
    };
    for (const auto &key: dict) {
        size_t index = text.find(key.second, 0);
        while (index != text.npos) {
            text.replace(index, key.second.size(), std::string(1, key.first));
            index = text.find(key.second, index + 1);
        }
    }
    return text;
}

EXMLItemType TXMLItem::GetType() const {
    return Type;
}
