#pragma once

#include <string>
#include <list>
#include <memory>
#include <unordered_map>
#include <fstream>
#include <iostream>
#include <sstream>
#include <exception>
#include <vector>

enum class EParserState {
    Text,
    TagName,
    WaitPropertyName,
    PropertyName,
    WaitPropertyValue,
    PropertyValue
};

enum class EXMLItemType {
    None,
    Text,
    Tag
};

class TXMLTag;
class TXMLText;

class TXMLItem {
    friend class TXMLTag;
public:
    operator TXMLTag&();
    operator TXMLText&();
    TXMLTag &ToTag();
    TXMLText &ToText();
    TXMLTag &GetParent();
    static std::string SerializeText(std::string text);
    static std::string DeserializeText(std::string text);
    EXMLItemType GetType() const;
protected:
    TXMLItem(EXMLItemType type = EXMLItemType::None):
        Type(type)
    {}
    virtual void Save(std::ostream &stream) = 0;
protected:
    TXMLTag *Parent = nullptr;
    EXMLItemType Type;
};

class TXMLText: public TXMLItem {
    friend class TXMLTag;
public:
    TXMLText(const TXMLText &other) = delete;
    void SetText(const std::string &text);
    TXMLText &operator =(const std::string &text);
    template <typename T> void SetText(T &value) {
        std::stringstream stream;
        stream << value;
        Text = stream.str();
    }
    bool IsComment() const;
    void SetComment(bool value);
    TXMLText &operator =(const TXMLText &other) = delete;
protected:
    TXMLText(const std::string &text = ""):
        TXMLItem(EXMLItemType::Text),
        Text(text)
    {}
    const std::string &GetText() const;
    void Save(std::ostream &stream) final;
private:
    std::string Text;
    bool Comment = false;
};

typedef std::list <std::unique_ptr<TXMLItem> >::iterator TXMLIterator;
typedef std::list <std::unique_ptr<TXMLItem> >::reverse_iterator TXMLReverseIterator;
typedef std::unordered_map <std::string, std::string>::iterator TXMLPropertyIterator;

class TXMLTag: public TXMLItem {
public:
    TXMLTag();
    TXMLTag(const TXMLTag &other);
    TXMLTag(const std::string &filename);
    void SaveToFile(const std::string &filename);
    std::string SaveToString();
    const std::string &GetProperty(const std::string &property);
    TXMLText &AddText(const std::string &text, TXMLIterator position);
    TXMLTag &AddTag(const std::string &name, TXMLIterator position);
    TXMLIterator Find(const std::string &name);
    TXMLIterator Find(const std::string &name, const TXMLIterator &position);
    void SetFromRawData(const std::string &string);
    void SetProperty(const std::string &name, const std::string &value);
    TXMLIterator Erase(const TXMLIterator &iterator);
    template <typename T> void SetProperty(const std::string &name, const T &value) {
        std::stringstream stream;
        stream << value;
        PropertyMap[name] = stream.str();
    }
    std::string &operator [](const std::string &name);
    TXMLTag &operator =(const std::string &rawData);
    TXMLTag &operator =(const TXMLTag &other);

    int GetPropertyCount() const;
    int GetItemCount() const;
    const std::string GetName() const;
    void SetName(const std::string &name);
public:
    TXMLIterator begin();
    TXMLIterator end();
    TXMLReverseIterator rbegin();
    TXMLReverseIterator rend();
    TXMLPropertyIterator begin_property();
    TXMLPropertyIterator end_property();
protected:
    void Save(std::ostream &stream) final;
private:
    void Parse(std::istream &stream);
private:
    std::string Name;
    std::list <std::unique_ptr<TXMLItem> > ItemList;
    std::unordered_map <std::string, std::string> PropertyMap;
};
