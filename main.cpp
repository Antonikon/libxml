#include <iostream>

#include "libxml.h"

using namespace std;

int main(int argc, char *argv[]) {
    TXMLTag file("sample.xml");
    TXMLTag &catalogue = **file.Find("catalogue");
    for (auto it = catalogue.begin(); it != catalogue.end(); ) {
        if ((**it).GetType() == EXMLItemType::Tag) {
            TXMLTag &item = **it;
            if (stoi(item["price"]) * (1.0 - stoi(item["discount"]) / 100.0) > 3000) {
                it = catalogue.Erase(it);
                continue;
            }
        }
        it++;
    }
    file.SaveToFile("result.xml");
    return 0;
}
